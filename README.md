##   Arbol Binario

##   Lenguaje:
  Python 3.6


##   Instalacion:
instalar pip3
```php
pip3 install flask
```
```php
pip3 install flask_restful
```

##   Ejecutar app:
```python
python3.6 api.py
```

##  Metodos:
##  Crear arbol
  url: http://localhost:5444/api/tree/create
  tipo: POST
  parametros: nodos 
  Nota: se debe enviar con espacio cada nodo para su creacion.


## Buscar Ancestro:
  url: http://0.0.0.0:5444/api/tree/search
  tipo: POST
  parametros: nodo1, nodo2


