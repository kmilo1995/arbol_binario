class node():
    #  Constructor clase nodo
    def __init__(self, dato):
        self.left = None
        self.right = None
        self.dato = dato

class tree: 
    #  Constructor clase arbol
    def __init__(self): 
        self.datas = None

    def insertar(self, array, dato):
        #Si el arbol aun no existe se crea la raiz
        if array == None:
            array = node(dato)
        else:
            d = array.dato
            #Si el nodo es menor a la raiz se inserta a la izquierda
            if dato < d:
                array.left = self.insertar(array.left, dato)
            else:
                #Si el nodo es mayor a la raiz se inserta a la derecha
                array.right = self.insertar(array.right, dato)
        return array
  
    # Funcion para encontrar el ancestro entre dos nodos
    def buscar(self,elemento, n1, n2): 
        
        if elemento is None: 
            return None
    
        # Si nodo1 y nodo2 son mas pequenos que la raiz se encuentra a la izquierda
        if(elemento.dato > n1 and elemento.dato > n2): 
            return self.buscar(elemento.left, n1, n2) 
    
        # Si nodo1 y nodo2 son mas grandes que la raiz se encuentra a la derecha
        if(elemento.dato < n1 and elemento.dato < n2): 
            return self.buscar(elemento.right, n1, n2) 
    
        return elemento 