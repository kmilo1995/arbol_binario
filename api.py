from flask import Flask, jsonify
from flask_restful import reqparse, abort, Api, Resource
from tree import tree

# Se crea las variables de entorno
app = Flask(__name__)
api = Api(app)

#Se instancia la clase tree, que contiene los metodos insertar y buscar el AB
obj = tree()

# Se crea las variables que recibira el api
parser = reqparse.RequestParser()
parser.add_argument('nodo1', type=str)
parser.add_argument('nodo2', type=str)
parser.add_argument('nodos', type=str)


class Arbol(Resource):
    def post(self):
        #Se crea el metodo Post que crea el arbol, mediante un espacio en el campo nodos.
        args = parser.parse_args()
        nodos = str(args['nodos'])
        nodos = nodos.split(' ')
        for item in nodos:
            obj.datas = obj.insertar(obj.datas,int(item))
        return jsonify(success=True,message='Arbol creado')

class Busqueda(Resource):
    def post(self):
        #Se crea el metodo Post que crea busca el ancestro de dos nodos.
        args = parser.parse_args()
        n1 = int(args['nodo1'])
        n2 = int(args['nodo2'])
        t = obj.buscar(obj.datas, n1, n2) 
        if(t==None):
            return jsonify(success=False, message=("No hay un arbol creado"))
        else:
            return jsonify(success=True, message=("El ancestro de %d y %d es: %d" %(n1, n2, t.dato)))


api.add_resource(Arbol, '/api/tree/create')
api.add_resource(Busqueda, '/api/tree/search')

if __name__ == '__main__':
    #inicio del restful
    app.run(host='0.0.0.0', port=5444)


